var server = require('../../server/server');
module.exports = function(Employee) {


  Employee.validatesUniquenessOf('email', {message: 'Email could be unique'});
  Employee.validatesUniquenessOf('employeeId',{message: 'employeeId could be unique'});
  Employee.observe('before save', function (ctx, next) {
    if (ctx.instance != undefined && ctx.instance != null) {
      ctx.instance.email=(ctx.instance.email.toLowerCase());
      ctx.instance.employeeId=(ctx.instance.employeeId.toLowerCase());
      ctx.instance.createdTime = new Date();
      next();
    } else {
      if(ctx.data.email!=undefined && ctx.data.email!=null && ctx.data.email!=''){
        ctx.data.email=(ctx.data.email.toLowerCase());
      }
      if(ctx.data.employeeId!=undefined && ctx.data.employeeId!=null && ctx.data.employeeId!=''){
        ctx.data.employeeId=(ctx.data.employeeId.toLowerCase());
      }
      ctx.data.updatedTime = new Date();
      next();
    }
  });

  Employee.remoteMethod('forgotPassword', {
    description: "Send Valid Details",
    returns: {
      arg: 'data',
      type: "object"
    },
    accepts: [{arg: 'searchData', type: 'object', http: {source: 'query'}}],
    http: {
      path: '/forgotPassword',
      verb: 'GET'
    }
  });

  Employee.forgotPassword = function (data, cb) {
    Employee.findOne({'where':{'email':data.emailId}},function (err, email) {
      if(err)console.log('error message is '+err);
      if(email!=undefined && email!=null){
        var randomstring = require("randomstring");
        var uniqueId = randomstring.generate({
          length: 6,
          charset: 'alphanumeric'
        });
        console.log("uniqueId "+uniqueId);
        var otp = uniqueId;
        var otpStatus = false;
        var updateData={
          password:otp,
          confirmPassword:otp
        };
        email.updateAttributes(updateData,function (err,updated) {
          var EmailTemplete = server.models.EmailTemplete;
          var Employee = server.models.Employee;
            var Emailtemplete = server.models.EmailTemplete;
            Emailtemplete.find({"where":{"emailType":"scheme"}},function (err, emailTemplete) {
              var Dhanbademail = server.models.DhanbadEmail;
              Dhanbademail.create({
                "to": data.emailId,
                "subject": emailTemplete[0].adminSubject,
                "text": emailTemplete[0].adminText+" "+otp
              }, function (err, email) {
              });

              if(emailTemplete[0].adminSMS && email.mobile) {
                var Sms = server.models.Sms;
                var smsData = {
                  "message": emailTemplete[0].adminSMS,
                  "mobileNo": email.mobile,
                  "smsservicetype": "singlemsg"
                };
                Sms.create(smsData, function (err, smsInfo) {
                });
              }
            });
          cb(null,updated);

        });
      }else{
        var message={
          message:'Please Register With US'
        };
        cb(null, message);
      }
    });
  };
};
