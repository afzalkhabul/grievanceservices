var server = require('../../server/server');
module.exports = function(Request) {
  Request.observe('before save', function (ctx, next) {

    if(ctx.data!=undefined && ctx.data!=null){
      ctx.data['updatedTime']=new Date();
      var Sms = server.models.Sms;

      if(ctx.data.requestStatus!=undefined && ctx.data.requestStatus!=null && ctx.data.requestStatus=='Approval'){
        var EmailTemplete = server.models.EmailTemplete;
        EmailTemplete.find({},function (err, emailTemplete) {
          console.log('data is : '+ emailTemplete[0].emailId+ ' customer emil is is '+ctx.data.email);
          if(emailTemplete!=null &&emailTemplete.length>0) {
            var nodemailer = require('nodemailer');
            var smtpTransport = require('nodemailer-smtp-transport');
            var transporter = nodemailer.createTransport(smtpTransport({
              host: "smtp.gmail.com",
              port: 465,
              secure: true,
              auth: {
                user: emailTemplete[0].emailId,
                pass: 'admin@admin'
              }
            }));
            var message = emailTemplete[0].requestApprovalMessage + '<br>Reason is ' + ctx.data.comment;
            var mailOptions = {
              from: emailTemplete[0].emailId,
              to: ctx.data.email,
              subject: emailTemplete[0].requestApprovalEmail, // Subject line
              text: '', // plaintext body
              html: message // html body
            };
            transporter.sendMail(mailOptions, function (error, info) {
              if (error) {
                return console.log(error);
              }
            });
            if(emailTemplete[0].requestApprovalSMS && ctx.data.mobileNumber) {
              var smsData = {
                "message": emailTemplete[0].requestApprovalSMS,
                "mobileNo": ctx.data.mobileNumber,
                "smsservicetype": "singlemsg"
              };

              Sms.create(smsData, function (err, smsInfo) {
              });
            }
          }
        });

      }
      else if(ctx.data.requestStatus!=undefined && ctx.data.requestStatus!=null && ctx.data.requestStatus=='Rejected'){
        var EmailTemplete = server.models.EmailTemplete;
        EmailTemplete.find({},function (err, emailTemplete) {
          if(emailTemplete!=null &&emailTemplete.length>0) {
            var nodemailer = require('nodemailer');
            var smtpTransport = require('nodemailer-smtp-transport');
            var transporter = nodemailer.createTransport(smtpTransport({
              host: "smtp.gmail.com",
              port: 465,
              secure: true,
              auth: {
                user: emailTemplete[0].emailId,
                pass: 'admin@admin'
              }
            }));
            var message = emailTemplete[0].rejectedMessage + '<br>Reason is ' + ctx.data.comment;
            var mailOptions = {
              from: emailTemplete[0].emailId,
              to: ctx.data.email,
              subject: emailTemplete[0].rejectedEmail, // Subject line
              text: '', // plaintext body
              html: message // html body
            };
            transporter.sendMail(mailOptions, function (error, info) {
              if (error) {
                return console.log(error);
              }
              console.log('Message sent: ' + info.response);
            });
            if(emailTemplete[0].rejectedSMS && ctx.data.mobileNumber) {
              var smsData = {
                "message": emailTemplete[0].rejectedSMS,
                "mobileNo": ctx.data.mobileNumber,
                "smsservicetype": "singlemsg"
              };

              Sms.create(smsData, function (err, smsInfo) {
                console.log('SMS info:' + JSON.stringify(smsInfo));
              });
            }
          }
        });
      }
      next();
    }
    else {
          checkUniqueId(ctx,next);
      ctx.instance.createdTime=new Date();
    }
  });
  function checkUniqueId(ctx,next){
    var randomstring = require("randomstring");
    var uniqueId = randomstring.generate({
      length: 7,
      charset: 'alphanumeric'
    });
    var date=new Date();
    var month;
    var dateIS;
    if((date.getMonth()+1)<10){
      month='0'+(date.getMonth()+1);
    }else{
      month=(date.getMonth()+1);
    }
    if(date.getDate()<10){
      dateIS='0'+date.getDate();
    }else{
      dateIS=date.getDate();
    }
    var string=date.getFullYear()+''+month+dateIS;
    console.log('date is '+date.getFullYear()+'data'+string);
    uniqueId=string+uniqueId;
    Request.find({"where": {"requestId": uniqueId}}, function (err, customers) {
      if(customers.length==0){
        ctx.instance.requestId = uniqueId;
        var WorkflowForm=server.models.WorkflowForm;
        var Workflow=server.models.Workflow;
        var WorkflowEmployees=server.models.WorkflowEmployees;
        var notification=server.models.notification;
        WorkflowForm.findOne({"where": {"schemeUniqueId": ctx.instance.schemeUniqueId}}, function (err, workflowForm) {
          console.log('work form '+JSON.stringify(workflowForm));
          if(workflowForm!=undefined && workflowForm!=null ){
            Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
              WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
                var workflowData = [];
                var employeeDetailsForNotification=[];
                  if (listData != undefined && listData != null && listData.length > 0) {
                  for (var i = 0; i < listData.length; i++) {
                    var flowData = {
                      workflowId: listData[i].workflowId,
                      levelId: listData[i].levelId,
                      status: listData[i].status,
                      maxLevels: listData[i].maxLevels,
                      levelNo: listData[i].levelNo
                    }
                    workflowData.push(flowData);
                    if(listData[i].levelNo=="1"){
                      if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                        for(var j=0;j<listData[i].employees.length;j++){
                          employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                        }
                      }
                    }
                  }
                }
                ctx.instance.workflow = workflowData;
                ctx.instance.acceptLevel = 0;
                ctx.instance.finalStatus = false;
                ctx.instance.workflowId = workflowForm.workflowId;
                ctx.instance.maxlevel = flowDataList.maxLevel;
                ctx.instance.lastUpdateTime=new Date();
                ctx.instance.createdTime = new Date();
                if(employeeDetailsForNotification.length>0){
                  notification.create({
                    'to':employeeDetailsForNotification,
                    "subject":  'New Tasks',
                    "text":"You got New Request",
                    "message":"You got New Request",
                    'urlData':'requests',
                    "type":"newRequest"
                  }, function (err, notificationDetails){
                    next();
                  });

                }else{
                  next();
                }
              });

            });
          }
        });
      }else{
        checkUniqueId(ctx,next);
      }
    });
  }


  Request.observe('loaded', function (ctx, next) {
    if(ctx.instance){
      if(ctx.instance.workflowId!=undefined && ctx.instance.workflowId!=null && ctx.instance.workflowId!='' ){
        var WorkflowEmployees=server.models.WorkflowEmployees;
        WorkflowEmployees.find({'where':{'and':[{'workflowId':ctx.instance.workflowId},{"status": "Active"}]}}, function (err, workflowEmployeeList) {
          if(workflowEmployeeList!=null && workflowEmployeeList.length>0){
            ctx.instance.workflowData=workflowEmployeeList;
            next();
          }else{
            next();
          }
        })
      }else{
        next();
      }
    }else{
      next();
    }
  });



  Request.getDetails = function (employee, cb) {
     Request.find({'where':{'finalStatus':false}},function (err, requestList) {
      var listRequest=[];
      if(requestList!=undefined && requestList!=null && requestList.length>0){
        var Employee=server.models.Employee;
        var adminStatus=false;
        Employee.find({'where':{"employeeId":employee.employeeId}}, function (err, employeeList) {
          if(employeeList!=null && employeeList.length>0){
            var firstEmployeeDetails=employeeList[0];
            if(firstEmployeeDetails.role=='superAdmin' || firstEmployeeDetails.role=='schemeAdmin'){
              adminStatus=true;
            }
            for(var i=0;i<requestList.length;i++){
              var data=requestList[i];
              var workflowDetails=data.workflowData;
              var approveFiledVisit=false;
              var acceptLevel=data.acceptLevel;
              if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
                for(var j=0;j<workflowDetails.length;j++){
                  var accessFiledVisit=false;
                  if(workflowDetails[j].employees!=undefined && workflowDetails[j].employees!=null && workflowDetails[j].employees.length>0 ){

                    var employeeList=workflowDetails[j].employees;
                    if(employeeList!=undefined && employeeList!=null && employeeList.length>0 ){
                      for(var x=0;x<employeeList.length;x++){
                        if(employeeList[x]==employee.employeeId){
                          accessFiledVisit=true;
                          break;
                        }
                      }
                    }
                    if(accessFiledVisit || adminStatus){
                      var workFlowLevel=parseInt(workflowDetails[j].levelNo);
                      if(acceptLevel==(workFlowLevel-1)){
                        approveFiledVisit=true;
                        break;
                      }
                    }
                  }
                }
              }
              if(accessFiledVisit || adminStatus){
                data.editStatus=approveFiledVisit;
                if(accessFiledVisit){
                  data.availableStatus=accessFiledVisit;
                }else if(adminStatus) {
                  data.availableStatus = adminStatus;
                }
                listRequest.push(data);
              }
            }
            cb(null,listRequest);

          }else{
            cb(null,listRequest);
          }
        });


      }else{
        cb(null,listRequest);
      }
    });
  };

  Request.remoteMethod('getDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object"
    },
    accepts: [{arg: 'employeeId', type: 'object', http: {source: 'query'}}],
    http: {
      path: '/getDetails',
      verb: 'GET'
    }
  });


  Request.updateDetails = function (request, cb) {
      Request.findOne({'where':{'id':request.requestId}},function (err, requestDetails) {
        if(requestDetails!=undefined  && requestDetails!=null){

          if(request.acceptLevel!=null){
            var acceptLevelStatus=false;
            var acceptLevel=request.acceptLevel;
            var rejectStatus=false;
            var finalApproval=false;
            var workflowList=[];
            var workflowData=requestDetails.workflow;
            if(workflowData!=undefined && workflowData!=null && workflowData.length>0){
              for(var i=0;i<workflowData.length;i++){
                var levelNo=parseInt(workflowData[i].levelNo);
                var maximumLevel=parseInt(workflowData[i].maxLevels);
                if(levelNo==(acceptLevel+1)){
                  console.log('both are same');
                  acceptLevelStatus=true;
                  if((acceptLevel+1)==maximumLevel){
                    finalApproval=true;
                  }
                  if(request.acceptStatus=='Yes'){
                    workflowData[i].acceptStatus=request.acceptStatus;
                    workflowData[i].requestStatus=request.requestStatus;
                    workflowData[i].comment=request.comment;
                    workflowData[i].employeeId=request.employeeId;
                    workflowData[i].employeeName=request.employeeName;
                    workflowList.push(workflowData[i]);
                  }else{
                    rejectStatus=true;
                    workflowData[i].acceptStatus=request.acceptStatus;
                    workflowData[i].requestStatus=request.requestStatus;
                    workflowData[i].comment=request.comment;
                    workflowData[i].employeeId=request.employeeId;
                    workflowData[i].employeeName=request.employeeName;
                    workflowList.push(workflowData[i]);
                  }


                }else{
                  workflowList.push(workflowData[i]);

                }
              }

            }
            if(acceptLevelStatus==true){
              var updatedData={};
              if(rejectStatus){
                updatedData.acceptLevel=request.acceptLevel+1;
                updatedData.workflow=workflowList;
                updatedData.finalStatus='Rejected';
                updatedData.requestStatus='Rejected';
                updatedData.acceptStatus='No';
                updatedData.lastUpdateTime=new Date();
                updatedData.comment=request.comment;
              }else{
                updatedData.acceptLevel=request.acceptLevel+1;
                updatedData.workflow=workflowList;
                updatedData.lastUpdateTime=new Date();
                if(finalApproval){
                  updatedData.finalStatus='Approved';
                  updatedData.requestStatus='Approval';
                  updatedData.acceptStatus='Yes';
                  updatedData.comment=request.comment;
                }
              }
              requestDetails.updateAttributes(updatedData,function (err, updatedDetails) {
                 var Sms = server.models.Sms;
                  if (updatedDetails.finalStatus != undefined && updatedDetails.finalStatus != "Approved" && updatedDetails.finalStatus != "Rejected") {
                    var Emailtemplete = server.models.EmailTemplete;
                    Emailtemplete.find({},function (err, emailTemplete) {
                      var Dhanbademail = server.models.DhanbadEmail;
                      Dhanbademail.create({
                        "to": updatedDetails.emailId,
                        "subject": emailTemplete[0].levelEmail,
                        "text": emailTemplete[0].levelText
                      }, function (err, emailId) {
                      });
                      if(emailTemplete[0].levelSMS && updatedDetails.mobileNumber) {
                        var smsData = {
                          "message": emailTemplete[0].levelSMS,
                          "mobileNo": updatedDetails.mobileNumber,
                          "smsservicetype": "singlemsg"
                        };
                        Sms.create(smsData, function (err, smsInfo) {
                        });
                      }
                    });



                  } else if (updatedDetails.finalStatus != undefined && updatedDetails.finalStatus == "Approved") {
                    var Emailtemplete = server.models.EmailTemplete;
                    Emailtemplete.find({},function (err, emailTemplete) {
                      var Dhanbademail = server.models.DhanbadEmail;
                      Dhanbademail.create({
                        "to": updatedDetails.emailId,
                        "subject": emailTemplete[0].requestApprovalEmail,
                        "text": emailTemplete[0].requestApprovalMessage
                      }, function (err, emailId) {
                      });
                      if(emailTemplete[0].requestApprovalSMS && updatedDetails.mobileNumber) {
                        var smsData = {
                          "message": emailTemplete[0].requestApprovalSMS,
                          "mobileNo": updatedDetails.mobileNumber,
                          "smsservicetype": "singlemsg"
                        };
                        Sms.create(smsData, function (err, smsInfo) {
                        });
                      }
                    });

                  } else if (updatedDetails.finalStatus != undefined && updatedDetails.finalStatus == "Rejected") {
                    var Emailtemplete = server.models.EmailTemplete;
                    Emailtemplete.find({},function (err, emailTemplete) {
                      var Dhanbademail = server.models.DhanbadEmail;
                      Dhanbademail.create({
                        "to": updatedDetails.emailId,
                        "subject": emailTemplete[0].rejectedEmail,
                        "text": emailTemplete[0].rejectedMessage
                      }, function (err, emailId) {

                      });
                      if(emailTemplete[0].rejectedSMS && updatedDetails.mobileNumber) {
                        var smsData = {
                          "message": emailTemplete[0].rejectedSMS,
                          "mobileNo": updatedDetails.mobileNumber,
                          "smsservicetype": "singlemsg"
                        };
                        Sms.create(smsData, function (err, smsInfo) {
                        });
                      }
                    });
                  }else{
                    if(updatedData.workflowId!=undefined && updatedData.workflowId!=null && updatedData.workflowId!=''){
                      var WorkflowEmployees=server.models.WorkflowEmployees;
                      var notification=server.models.notification;
                      WorkflowEmployees.find({'where':{'workflowId':updatedData.workflowId}},function (err, listData) {
                        var workflowData = [];
                        var employeeDetailsForNotification=[];
                        if (listData != undefined && listData != null && listData.length > 0) {
                          for (var i = 0; i < listData.length; i++) {
                            if(parseInt(listData[i].levelNo)==(updatedData.acceptLevel+1)){
                              if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                                for(var j=0;j<listData[i].employees.length;j++){
                                  employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                                }
                              }
                            }
                          }
                        }
                        if(employeeDetailsForNotification.length>0){
                          notification.create({
                            'to':employeeDetailsForNotification,
                            "subject":  'New Tasks',
                            "text":"You got New Scheme Request",
                            "message":"You got New Scheme Request",
                            'urlData':'requests',
                            "type":"newRequest"
                          }, function (err, notificationDetails){
                            cb(null, updatedData);
                          });

                        }else{
                          cb(null, updatedData);
                        }
                      });
                    }else{
                      cb(null, updatedData);
                    }
                  }
                //cb(null,updatedDetails);
                //console.log('updated list'+JSON.stringify(updatedDetails));
              })
            }
          }
        }
      })
  };

  Request.remoteMethod('updateDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object"
    },
    accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateDetails',
      verb: 'POST'
    }
  });

  //After Save Method
  Request.observe('after save', function (ctx, next) {
    var Request = server.models.Request;
    if(ctx.isNewInstance){

      var Emailtemplete = server.models.EmailTemplete;
      Emailtemplete.find({},function (err, emailTemplete) {
        var Dhanbademail = server.models.DhanbadEmail;
        Dhanbademail.create({
          "to": ctx.instance.emailId,
          "subject": emailTemplete[0].requestEmail,
          "text": emailTemplete[0].requestText
        }, function (err, emailId) {
        });
        if(emailTemplete[0].registerSMS && ctx.instance.mobileNumber) {
          var Sms = server.models.Sms;
          var smsData = {
            "message": emailTemplete[0].registerSMS,
            "mobileNo": ctx.instance.mobileNumber,
            "smsservicetype": "singlemsg"
          };
          Sms.create(smsData, function (err, smsInfo) {
          });
        }
        next();
      });
    } else {
      next();
    }
  });

};
