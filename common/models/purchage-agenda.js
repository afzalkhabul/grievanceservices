var server = require('../../server/server');

module.exports = function(PurchageAgenda) {

  PurchageAgenda.observe('before save', function (ctx, next) {

    if(ctx.instance!=undefined && ctx.instance!=null){
      ctx.instance.createdTime = new Date();

      var randomString = require("randomstring");

      var meetingDate = ctx.instance.meetingDate.split('-').join('/');

      var department =ctx.instance.departmentName.toUpperCase();

      var purchaseAgendaNumber = randomString.generate({
        length: 4,
        charset: 'alphanumeric'
      });

      ctx.instance['purchaseAgendaNumber'] = meetingDate+"/"+department.substring(0,3)+"/"+purchaseAgendaNumber.toUpperCase();
      next();
    } else {

      ctx.data.updatedTime = new Date();
      next();

    }
  });

  /*vehicle purchase request aging remote method start*/

      PurchageAgenda.getVehiclePendingRequests = function (cb) {
       var VehiclePurchaseRequest = server.models.VehiclePurchaseRequest;
       PurchageAgenda.find({}, function (err, vehicleRequestLength) {

       console.log("data is" +JSON.stringify(vehicleRequestLength))
         var finalObject = [];
         if (vehicleRequestLength != undefined && vehicleRequestLength.length > 0) {
           var x = 0;
           var tenDaysCount = 0
           var oneMonthCount = 0
           var threeMonthCount = 0
           //var totalCount = 0
           var count = 0;
           var startDate;
           var endDate;
           for (var x = 0; x < vehicleRequestLength.length; x++) {

             //console.log('qureyData'+schemeLength[x].schemeUniqueId);
             startDate=new Date(new Date().getTime() - (10 * 24 * 60 * 60 * 1000));
             var queryData = [{'purchaseAgendaNumber': vehicleRequestLength[x].purchaseAgendaNumber}, {'createdTime': {'gt': startDate}}, {"finalStatus": false}]

             VehiclePurchaseRequest.count({'and': queryData}, function (err, reports) {
               console.log('first one' + JSON.stringify(reports));
               tenDaysCount = reports;
               //cb(null,{'details':falseCount});
             });
             startDate=new Date(new Date().getTime() - (30 * 24 * 60 * 60 * 1000));
             endDate=new Date(new Date().getTime() - (10 * 24 * 60 * 60 * 1000));
             queryData = [{'purchaseAgendaNumber': vehicleRequestLength[x].purchaseAgendaNumber}, {'createdTime': {'gt': startDate}},{'createdTime': {'lt': endDate}}, {"finalStatus": false}];
             VehiclePurchaseRequest.count({'and': queryData}, function (err, approved) {
               console.log('Secound one' + JSON.stringify(approved));
               oneMonthCount = approved;

             });
             startDate=new Date(new Date().getTime() - (90 * 24 * 60 * 60 * 1000));
             endDate=new Date(new Date().getTime() - (30 * 24 * 60 * 60 * 1000));
             queryData = [{'purchaseAgendaNumber': vehicleRequestLength[x].purchaseAgendaNumber}, {'createdTime': {'gt': startDate}},{'createdTime': {'lt': endDate}}, {"finalStatus": false}]
             VehiclePurchaseRequest.count({'and': queryData}, function (err, reject) {
               threeMonthCount = reject;
               console.log('thrid one' + JSON.stringify(reject));
               //totalCount = falseCount + approvedCount + rejectedCount;
               var data = {
                 'vehicleRequestDetails': vehicleRequestLength[count],
                 'tenDaysCount': tenDaysCount,
                 'oneMonthCount': oneMonthCount,
                 'threeMonthCount': threeMonthCount
               }
               finalObject.push(data);
               count++;
               if (vehicleRequestLength.length == count) {
                 cb(null, finalObject);
               }
             });
           }

         }else{
           cb(null,[]);
         }
       });

       };

      PurchageAgenda.remoteMethod('getVehiclePendingRequests', {
        description: "Send Valid EmployeeId",
        returns: {
          arg: 'data',
          type: "object",
          root:true
        },
        accepts: [],
        http: {
          path: '/getVehiclePendingRequests',
          verb: 'GET'
        }
      });




    /*vehicle purchase request aging remote method end*/


    /*Get Purchase Agenda Wise Report start*/

      PurchageAgenda.getAgendaWiseReport = function (dates, cb) {
        var VehiclePurchaseRequest = server.models.VehiclePurchaseRequest;
        if(dates.duration!=undefined && dates.duration!=null ){
           var startDate;
           var endDate;

          var todayDate=new Date();

          console.log("dates are" +dates)

            if(dates.duration=='1'){
              startDate=new Date(new Date() - (7 * 24 * 60 * 60 * 1000));
            } else  if(dates.duration=='2'){
              startDate=new Date(new Date() - (30 * 24 * 60 * 60 * 1000));
            }else{
              startDate=new Date(new Date() - (60 * 24 * 60 * 60 * 1000));
            }
          PurchageAgenda.find({}, function (err,purchaseAgendaLength) {
            var finalObject=[];
            if(purchaseAgendaLength!=undefined && purchaseAgendaLength.length>0){
              var x = 0;
              var falseCount=0
              var approvedCount=0
              var rejectedCount=0
              var totalCount=0
              var count=0;
              for (var x=0;x < purchaseAgendaLength.length;x++) {

                var queryData=[{'purchaseAgendaNumber':purchaseAgendaLength[x].purchaseAgendaNumber},{'createdTime': {'gt': startDate}},{"finalStatus" : false}]

                VehiclePurchaseRequest.count({'and':queryData}, function (err, reports) {
                  console.log('first one'+JSON.stringify(reports));
                  falseCount=reports;
                  //cb(null,{'details':falseCount});
                });
                queryData=[{'purchaseAgendaNumber':purchaseAgendaLength[x].purchaseAgendaNumber},{'createdTime': {'gt': startDate}},{"finalStatus" : 'Approved'}];
                VehiclePurchaseRequest.count({'and':queryData}, function (err, approved) {
                  console.log('Secound one'+JSON.stringify(approved));
                  approvedCount=approved;

                });
                queryData=[{'purchaseAgendaNumber':purchaseAgendaLength[x].purchaseAgendaNumber},{'createdTime': {'gt': startDate}},{"finalStatus" : 'Rejected'}]
                VehiclePurchaseRequest.count({'and':queryData}, function (err, reject) {
                  rejectedCount=reject;
                  console.log('thrid one'+JSON.stringify(reject));
                  totalCount=falseCount+approvedCount+rejectedCount;
                  var data={
                    'purchaseAgendaDetails':purchaseAgendaLength[count],
                    'falseCount':falseCount,
                    'approvedCount':approvedCount,
                    'rejectCount':rejectedCount,
                    'totalCount':totalCount
                  }
                  finalObject.push(data);
                  count++;
                  if(purchaseAgendaLength.length==count){
                    cb(null,finalObject);
                  }
                });
              }

            }
          });

        }else if(dates.startDate!=undefined && dates.startDate!=null && dates.endDate!=undefined && dates.endDate!=null ){
            startDate=new  Date(dates.startDate);
            endDate=new  Date(dates.endDate);
          Scheme.find({}, function (err,purchaseAgendaLength) {
            var finalObject=[];
            if(purchaseAgendaLength!=undefined && purchaseAgendaLength.length>0){
              var x = 0;
              var falseCount=0
              var approvedCount=0
              var rejectedCount=0
              var totalCount=0
              var count=0;
              for (var x=0;x < purchaseAgendaLength.length;x++) {

                //console.log('qureyData'+schemeLength[x].schemeUniqueId);
                var queryData=[{'purchaseAgendaNumber':purchaseAgendaLength[x].purchaseAgendaNumber},{'createdTime': {'gt': startDate}},{'createdTime': {'lt': endDate}},{"finalStatus" : false}]

                VehiclePurchaseRequest.count({'and':queryData}, function (err, reports) {
                  console.log('first one'+JSON.stringify(reports));
                  falseCount=reports;
                  //cb(null,{'details':falseCount});
                });
                queryData=[{'purchaseAgendaNumber':purchaseAgendaLength[x].purchaseAgendaNumber},{'createdTime': {'gt': startDate}},{'createdTime': {'lt': endDate}},{"finalStatus" : 'Approved'}];
                VehiclePurchaseRequest.count({'and':queryData}, function (err, approved) {
                  console.log('Secound one'+JSON.stringify(approved));
                  approvedCount=approved;

                });
                queryData=[{'purchaseAgendaNumber':purchaseAgendaLength[x].purchaseAgendaNumber},{'createdTime': {'gt': startDate}},{'createdTime': {'lt': endDate}},{"finalStatus" : 'Rejected'}]
                VehiclePurchaseRequest.count({'and':queryData}, function (err, reject) {
                  rejectedCount=reject;
                  console.log('thrid one'+JSON.stringify(reject));
                  totalCount=falseCount+approvedCount+rejectedCount;
                  var data={
                    'schmeDetails':purchaseAgendaLength[count],
                    'falseCount':falseCount,
                    'approvedCount':approvedCount,
                    'rejectCount':rejectedCount,
                    'totalCount':totalCount
                  }
                  finalObject.push(data);
                  count++;
                  if(purchaseAgendaLength.length==count){
                    cb(null,finalObject);
                  }
                });
              }

            }
          });
        }else{
          var error = new Error('Please provide valid details');
          error.statusCode = 200;
          cb(null,error);
        }
        //startDate=new Date(new Date().getTime() - (7 * 24 * 60 * 60 * 1000));

       };

      PurchageAgenda.remoteMethod('getAgendaWiseReport', {
        description: "Send Valid EmployeeId",
        returns: {
          arg: 'data',
          type: "object",
          root:true
        },
        accepts: [{arg: 'filter', type: 'object', http: {source: 'body'}}],
        http: {
          path: '/getAgendaWiseReport',
          verb: 'POST'
        }
      });


    /*Get Purchase Agenda Wise Report end*/




};

