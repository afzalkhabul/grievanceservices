var server = require('../../server/server');
module.exports = function(Casestakeholder) {

  Casestakeholder.observe('before save', function (ctx, next) {
    if(ctx.data!=undefined && ctx.data!=null){
      ctx.data['updatedTime']=new Date();
       next();
    }else {

      Casestakeholder.find({"where": {"email": ctx.instance.email,"caseNumber":ctx.instance.caseNumber}}, function (err, cases) {
        if (cases.length > 0) {
          var error = new Error('Your Already Assigned Stake Holder to This Case');
          error.statusCode = 200;
          next(error);
        }
      })
      ctx.instance.createdTime=new Date();
      next();
    }
  });
};
