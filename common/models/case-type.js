var server = require('../../server/server');

module.exports = function(Casetype) {
  Casetype.validatesUniquenessOf('caseType', {message: 'CaseType could be unique'});
  Casetype.observe('before save', function (ctx, next) {
    if (ctx.instance != undefined && ctx.instance != null) {
      ctx.instance.caseType=(ctx.instance.caseType.toLowerCase());
      ctx.instance.createdTime = new Date();
      next();
    } else {
      if(ctx.data.caseType!=undefined && ctx.data.caseType!=null && ctx.data.caseType!=''){
        ctx.data.caseType=(ctx.data.caseType.toLowerCase());
      }
      ctx.data.updatedTime = new Date();
      next();
    }
  });

};
