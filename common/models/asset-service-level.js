module.exports = function(AssetServiceLevel) {

  AssetServiceLevel.validatesUniquenessOf('description', {message: 'Asset Service Level Could Be Unique'});
  AssetServiceLevel.observe('before save', function (ctx, next) {
    if (ctx.instance != undefined && ctx.instance != null) {
              ctx.instance.description=(ctx.instance.description.toLowerCase());
              ctx.instance.createdTime = new Date();
              next();
            } else {
              if(ctx.data.description!=undefined && ctx.data.description!=null && ctx.data.description!=''){
                ctx.data.description=(ctx.data.description.toLowerCase());
              }
              ctx.data.updatedTime = new Date();
              next();
            }
  });

};
