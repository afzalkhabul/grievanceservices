var server = require('../../server/server');
module.exports = function(ProjectWorkOrders) {

  ProjectWorkOrders.observe('before save', function (ctx, next) {
    var WorkflowForm=server.models.WorkflowForm;
    var Workflow=server.models.Workflow;
    var WorkflowEmployees=server.models.WorkflowEmployees;
    var notification=server.models.notification;
    if(ctx.instance){
      ctx['createdTime']=new Date();
      WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "workOrder"}]}}, function (err, workflowForm) {
        if(workflowForm!=undefined && workflowForm!=null ){
          Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
            WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
              var workflowData = [];
              var employeeDetailsForNotification=[];
              if (listData != undefined && listData != null && listData.length > 0) {
                for (var i = 0; i < listData.length; i++) {
                  var flowData = {
                    workflowId: listData[i].workflowId,
                    levelId: listData[i].levelId,
                    status: listData[i].status,
                    maxLevels: listData[i].maxLevels,
                    levelNo: listData[i].levelNo

                  }
                  workflowData.push(flowData);
                  if(listData[i].levelNo=="1"){
                    if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                      for(var j=0;j<listData[i].employees.length;j++){
                        employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                      }
                    }
                  }
                }
              }
              ctx.instance.workflow = workflowData;
              ctx.instance.workflowId=workflowForm.workflowId;
              ctx.instance.maxlevel=flowDataList.maxLevel;
              ctx.instance.acceptLevel=0;
              ctx.instance.finalStatus=false;
              ctx.instance.createdTime=new Date();
              ctx.instance.lastUpdateTime=new Date();
              if(employeeDetailsForNotification.length>0){
                notification.create({
                  'to':employeeDetailsForNotification,
                  "subject":  'New Tasks',
                  "text":"You got New Plan Work Order  Request",
                  "message":"You got New Plan Work Order  Request",
                  'urlData':'projectWorkOrder',
                  "type":"newRequest"
                }, function (err, notificationDetails){
                  next();
                });
                var reviewPersonList=[{'employeeId':ctx.instance.employeeId,'readStatus':false}];

                if(reviewPersonList.length>0){
                  notification.create({
                    'to':reviewPersonList,
                    "subject":  'New Tasks',
                    "text":"Your Payment is applied successfully",
                    "message":"Your Payment is applied successfully",
                    'urlData':'paymentOrder',
                    "type":"newRequest"
                  });
                }
              }else{
                next();
              }
            });

          });
        }
      });
    }else{

      ctx['updatedTime']=new Date();
      next();
    }
  });

  ProjectWorkOrders.observe('loaded', function (ctx, next) {
    if(ctx.instance){
      if(ctx.instance.employeeId!=undefined && ctx.instance.employeeId!=null){
        var Employee=server.models.Employee;
        Employee.find({'where':{'employeeId':ctx.instance.employeeId}},function(err, employee){
          if(employee.length>0){
            ctx.instance.employeeName=employee[0].name;
          }
        });
      }
      if(ctx.instance.workflowId!=undefined && ctx.instance.workflowId!=null && ctx.instance.workflowId!='' ){
        var WorkflowEmployees=server.models.WorkflowEmployees;
        WorkflowEmployees.find({'where':{'and':[{'workflowId':ctx.instance.workflowId},{"status": "Active"}]}}, function (err, workflowEmployeeList) {
          if(workflowEmployeeList!=null && workflowEmployeeList.length>0){
            ctx.instance.workflowData=workflowEmployeeList;
            next();
          }else{
            next();
          }
        })
      }else{
        next();
      }
    }else{
      next();
    }
  });

  ProjectWorkOrders.getDetails = function (employee,planId, cb) {
    ProjectWorkOrders.find({"where":{"planId":planId}},function(err, projectWorkOrdersData){
      var projectWorkOrdersList=[];
      if(projectWorkOrdersData!=null && projectWorkOrdersData.length>0){

        var Employee=server.models.Employee;
        var adminStatus=false;
        Employee.find({'where':{"employeeId":employee}}, function (err, employeeList) {
          if(employeeList!=null && employeeList.length>0) {
            var firstEmployeeDetails = employeeList[0];
            if (firstEmployeeDetails.role == 'superAdmin' || firstEmployeeDetails.role == 'projectAdmin') {
              adminStatus = true;
            }
            for (var i = 0; i < projectWorkOrdersData.length; i++) {
              var data = projectWorkOrdersData[i];
              var workflowDetails = data.workflowData;
              var accessFiledVisit = false;
              var approveFiledVisit = false;
              if (workflowDetails != undefined && workflowDetails != null && workflowDetails.length > 0) {
                var acceptLevel = data.acceptLevel;
                var editDetails;
                if (data.createdPersonId == employee) {
                  editDetails = true;
                }
                var availableStatus = false;
                if (data.finalStatus == false) {
                  for (var j = 0; j < workflowDetails.length; j++) {
                    var availableStatusLevel = false;
                    if (workflowDetails[j].employees != undefined && workflowDetails[j].employees != null && workflowDetails[j].employees.length > 0) {
                      var employeeList = workflowDetails[j].employees;
                      if (employeeList != undefined && employeeList != null && employeeList.length > 0) {
                        for (var x = 0; x < employeeList.length; x++) {
                          if (employeeList[x] == employee) {
                            availableStatus = true;
                            availableStatusLevel = true;
                            break;
                          }
                        }
                      }
                      if (availableStatusLevel) {
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }else if(adminStatus){
                        availableStatus=true;
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                        console.log('workflow level' + workFlowLevel + " accepct level" + acceptLevel);
                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }

                    }

                  }
                  if (availableStatus || editDetails ) {
                    if (editDetails) {
                      if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                        data.editDetails = true;
                      } else {
                        data.editDetails = false;
                      }
                    }
                    data.approveFiledVisit = approveFiledVisit;
                    data.availableStatus = availableStatus;
                    projectWorkOrdersList.push(data);
                  }
                }else{
                  if (editDetails) {
                    if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                      data.editDetails = true;
                    } else {
                      data.editDetails = false;
                    }
                  }
                  data.approveFiledVisit = false;
                  data.availableStatus = true;
                  projectWorkOrdersList.push(data);
                }


              }
            }
            cb(null,projectWorkOrdersList);
          }else{
            cb(null,projectWorkOrdersList);
          }
          });

      }else{
        cb(null,projectWorkOrdersList);
      }
    });
  };


  ProjectWorkOrders.remoteMethod('getDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'employeeId', type: 'string', http: {source: 'query'},required:true},
      {arg: 'planId', type: 'string', http: {source: 'query'},required:true}],
    http: {
      path: '/getDetails',
      verb: 'GET'
    }
  });

  ProjectWorkOrders.updateDetails = function (approvalDetails, cb) {
    ProjectWorkOrders.findOne({'where':{'id':approvalDetails.requestId}},function(err, projectWorkOrdersDetails){
      if(projectWorkOrdersDetails!=undefined && projectWorkOrdersDetails!=null){
        var finalObject={};
        if(projectWorkOrdersDetails.acceptLevel!=undefined && projectWorkOrdersDetails.acceptLevel!=null && projectWorkOrdersDetails.acceptLevel==approvalDetails.acceptLevel){
          var workflowDetails=projectWorkOrdersDetails.workflow;
        if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
         var workflowData=[];
          for(var  i=0;i< workflowDetails.length;i++){
            var data=workflowDetails[i];
            var workFlowLevel=parseInt(workflowDetails[i].levelNo);
            if((approvalDetails.acceptLevel+1)==workFlowLevel){
                data.approvalStatus=approvalDetails.requestStatus;
                data.employeeName=approvalDetails.employeeName;
                data.employeeId=approvalDetails.employeeId;
                data.comment=approvalDetails.comment;
                data.date=new Date();
            }
            workflowData.push(data);

          }
          finalObject.workflow=workflowData;
          finalObject.lastUpdateTime=new Date();
        }
       if(approvalDetails.requestStatus=="Approval"){
         if(projectWorkOrdersDetails.maxlevel==(approvalDetails.acceptLevel+1)){
           finalObject.finalStatus=approvalDetails.requestStatus;
           finalObject.acceptLevel=approvalDetails.acceptLevel+1;

         }else{
           finalObject.acceptLevel=approvalDetails.acceptLevel+1;
         }

       } else if(approvalDetails.requestStatus=="Rejected"){
         finalObject.finalStatus=approvalDetails.requestStatus;
         finalObject.acceptLevel=approvalDetails.acceptLevel+1;
         finalObject.rejectComment=approvalDetails.comment;
       }
        var Sms = server.models.Sms;
        projectWorkOrdersDetails.updateAttributes(finalObject, function(err, finalData){
          if(finalObject.finalStatus!=undefined && finalObject.finalStatus!=null && finalObject.finalStatus=="Rejected" ){
            if(finalData.createdPersonId!=undefined && finalData.createdPersonId!=null && finalData.createdPersonId!='' ){
              var Employee=server.models.Employee;
              var EmailTemplete = server.models.EmailTemplete;
              Employee.find({'where':{'employeeId':finalData.createdPersonId}}, function (err, employeeDetails) {
                if(employeeDetails!=null && employeeDetails.length>0){

                  EmailTemplete.findOne({"where":{"emailType": "projectWardWorks"}},function (err, emailTemplete) {
                    if(emailTemplete!=null ) {
                      var message = emailTemplete.workOrderRejectedMessage + '<br>Reason is ' + approvalDetails.comment;
                      var Dhanbademail = server.models.DhanbadEmail;
                      var notification=server.models.notification;
                      notification.create({
                        'to':[{'employeeId':employeeDetails[0].employeeId,'readStatus':false}],
                        "subject":  'New Tasks',
                        "text":"Your project Work Order  Request is Rejected",
                        "message":"Your project Work Order   Request is Rejected",
                        'urlData':'projectWorkOrder',
                        "type":"rejectRequest"
                      }, function (err, notificationDetails){
                        console.log('after update new plan request rejected');
                      });
                      Dhanbademail.create({
                        from: emailTemplete.emailId,
                        to: employeeDetails[0].email,
                        subject: emailTemplete.workOrderRejectedEmail, // Subject line
                        "text":message
                      }, function (err, email) {
                      });

                      if(emailTemplete.workOrderRejectedSMS && employeeDetails[0].mobile) {
                        var smsData = {
                          "message": emailTemplete.workOrderRejectedSMS,
                          "mobileNo": employeeDetails[0].mobile,
                          "smsservicetype": "singlemsg"
                        };

                        Sms.create(smsData, function (err, smsInfo) {

                        });
                      }
                    }
                  });
                  cb(null, finalData);
                }else{
                  cb(null, finalData);
                }
              });

            }else{
              cb(null, finalData);
            }

          }
          else if(finalObject.finalStatus!=undefined && finalObject.finalStatus!=null && finalObject.finalStatus=="Approval"){

            if(finalData.createdPersonId!=undefined && finalData.createdPersonId!=null && finalData.createdPersonId!='' ){
              var Employee=server.models.Employee;
              var EmailTemplete = server.models.EmailTemplete;
              Employee.find({'where':{'employeeId':finalData.createdPersonId}}, function (err, employeeDetails) {
                if(employeeDetails!=null && employeeDetails.length>0){
                  EmailTemplete.findOne({"where":{"emailType": "projectWardWorks"}},function (err, emailTemplete) {
                    if(emailTemplete!=null) {
                      var message = emailTemplete.workOrderRequestApprovalMessage + '<br>Reason is ' + approvalDetails.comment;
                      var Dhanbademail = server.models.DhanbadEmail;
                      var notification=server.models.notification;
                      notification.create({
                        'to':[{'employeeId':employeeDetails[0].employeeId,'readStatus':false}],
                        "subject":  'New Tasks',
                        "text":"Your project work Order is approved",
                        "message":"Your project work Order Request is approved",
                        'urlData':'projectWorkOrder',
                        "type":"approvedRequest"
                      }, function (err, notificationDetails){
                        console.log('after update new plan request Approved');
                      });
                      Dhanbademail.create({
                        from: emailTemplete.emailId,
                        to: employeeDetails[0].email,
                        subject: emailTemplete.workOrderRequestApprovalEmail, // Subject line
                        "text":message
                      }, function (err, email) {

                      });

                      if(emailTemplete.workOrderRequestApprovalSMS && employeeDetails[0].mobile) {
                        var smsData = {
                          "message": emailTemplete.workOrderRequestApprovalSMS,
                          "mobileNo": employeeDetails[0].mobile,
                          "smsservicetype": "singlemsg"
                        };

                        Sms.create(smsData, function (err, smsInfo) {
                        });
                      }
                    }
                  });
                  cb(null, finalData);
                }else{
                  cb(null, finalData);
                }
              });
            }
          }
          else{
            if(finalData.workflowId!=undefined && finalData.workflowId!=null && finalData.workflowId!=''){
              var WorkflowEmployees=server.models.WorkflowEmployees;
              var notification=server.models.notification;
              WorkflowEmployees.find({'where':{'workflowId':finalData.workflowId}},function (err, listData) {
                var workflowData = [];
                var employeeDetailsForNotification=[];
                if (listData != undefined && listData != null && listData.length > 0) {
                  for (var i = 0; i < listData.length; i++) {
                    if(parseInt(listData[i].levelNo)==(finalData.acceptLevel+1)){
                      if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                        for(var j=0;j<listData[i].employees.length;j++){
                          employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                        }
                      }
                    }
                  }
                }
                if(employeeDetailsForNotification.length>0){
                  notification.create({
                    'to':employeeDetailsForNotification,
                    "subject":  'New Tasks',
                    "text":"You got New Project work Order Request",
                    "message":"You got New Project work Order Request",
                    'urlData':'projectWorkOrder',
                    "type":"newRequest"
                  }, function (err, notificationDetails){
                    cb(null, finalData);
                  });

                }else{
                  cb(null, finalData);
                }
              });
            }else{
              cb(null, finalData);
            }
          }
        });


        }else{
          var error = new Error('This level is already approved. Can you please refresh page');
          error.statusCode = 200;
          cb(error,null);
        }

      }else {
        var error = new Error('Your plan Field Visit details does not exits');
        error.statusCode = 200;
        cb(error,null);
      }

    });


  };

  ProjectWorkOrders.remoteMethod('updateDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateDetails',
      verb: 'POST'
    }
  });

  ProjectWorkOrders.updateContent= function (updatedDetails, cb) {
    ProjectWorkOrders.findOne({'where':{'id':updatedDetails.id}},function(err, projectWorkOrdersDetails){
      var WorkflowForm=server.models.WorkflowForm;
      var Workflow=server.models.Workflow;
      var WorkflowEmployees=server.models.WorkflowEmployees;
      var employeeDetailsForNotification=[];
      var notification=server.models.notification;
      WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "workOrder"}]}}, function (err, workflowForm) {

        if(workflowForm!=undefined && workflowForm!=null ){
          Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
            WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
              var workflowData = [];
              if (listData != undefined && listData != null && listData.length > 0) {
                for (var i = 0; i < listData.length; i++) {
                  var flowData = {
                    workflowId: listData[i].workflowId,
                    levelId: listData[i].levelId,
                    status: listData[i].status,
                    maxLevels: listData[i].maxLevels,
                    levelNo: listData[i].levelNo
                  }
                  workflowData.push(flowData);
                  if(listData[i].levelNo=="1"){
                    if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                      for(var j=0;j<listData[i].employees.length;j++){
                        employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                      }
                    }
                  }
                }
              }
              updatedDetails.workflow = workflowData;
              updatedDetails.workflowId=workflowForm.workflowId;
              updatedDetails.maxlevel=flowDataList.maxLevel;
              updatedDetails.acceptLevel=0;
              updatedDetails.finalStatus=false;
              updatedDetails.lastUpdateTime=new Date();
              updatedDetails['updatedTime']=new Date();
              notification.create({
                'to':employeeDetailsForNotification,
                "subject":  'New Tasks',
                "text":"You got edit Work Order Request",
                "message":"You got edit Work Order Request",
                'urlData':'projectWorkOrder',
                "type":"newRequest"
              }, function (err, notificationDetails){
              });
              projectWorkOrdersDetails.updateAttributes(updatedDetails,function(err,data){
              });
              cb(null, projectWorkOrdersDetails);
            });

          });
        }
      });
    });
  };

  ProjectWorkOrders.remoteMethod('updateContent', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateContent',
      verb: 'POST'
    }
  });

  ProjectWorkOrders.getProjectsData = function (cb) {
    var ProjectPlan=server.models.ProjectPlan;
    ProjectPlan.find({"where":{"finalStatus" : "Approval"}},function(err, projectWorkOrdersData){
      if(projectWorkOrdersData!=undefined && projectWorkOrdersData!=null && projectWorkOrdersData.length>0 ){
       var projectData=[];
        console.log('length is '+projectWorkOrdersData.length);
        for(var i=0;i<projectWorkOrdersData.length; i++){
          var statusCode;
          if(projectWorkOrdersData[i].taskStatus=='new'){
            statusCode=0;
          }else   if(projectWorkOrdersData[i].taskStatus=='pending'){
            statusCode=1;
          }else  if(projectWorkOrdersData[i].taskStatus=='working'){
            statusCode=2;
          }else  if(projectWorkOrdersData[i].taskStatus=='completed'){
            statusCode=3;
          }else  if(projectWorkOrdersData[i].taskStatus=='reBaseline'){
            statusCode=4;
          }else  if(projectWorkOrdersData[i].taskStatus=='closed'){
            statusCode=5;
          }
          projectData.push({
            'planName':projectWorkOrdersData[i].name,
            'status':statusCode,
            'coordinates':projectWorkOrdersData[i].locationCoOrdinates
          });
       }
        cb(null,projectData);
      }else{
        cb(null,projectWorkOrdersData)
      }

    });
  };


  ProjectWorkOrders.remoteMethod('getProjectsData', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [],
    http: {
      path: '/getProjectsInfo',
      verb: 'GET'
    }
  });



};
