var server = require('../../server/server');
module.exports = function(Advocate) {
  Advocate.validatesUniquenessOf('email', {message: 'Email could be unique'});
  Advocate.validatesUniquenessOf('advId', {message: 'Advocate Id could be unique'});
  Advocate.observe('before save', function (ctx, next) {
    if (ctx.instance != undefined && ctx.instance != null) {
      ctx.instance.advId=(ctx.instance.advId.toLowerCase());
      ctx.instance.createdTime = new Date();
      next();
    } else {
      if(ctx.data.advId!=undefined && ctx.data.advId!=null && ctx.data.advId!=''){
        ctx.data.advId=(ctx.data.advId.toLowerCase());
      }
      ctx.data.updatedTime = new Date();
      next();



    }
  });
};
